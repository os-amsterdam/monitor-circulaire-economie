# Amsterdam Circular Monitor

## Introduction
This is the codebase used by the City of Amsterdam to calculate the circular economy metrics shown in the [Amsterdam Circular Monitor](https://onderzoek.amsterdam.nl/dossier/circulaire-economie). In particular, this project performs the following operations:
- Preprocessing two main datasets (see below)
- Calculating metrics for materials and waste
- Estimating environmental impact of materials consumption

## Disclaimer
The methods and data used in this project are new and come with a degree of uncertainty. Some assumptions are probably wrong and should be improved as better data becomes available. We've tried to be as explicit as possible about these assumptions to invite critical feedback and suggestions for improvement. However, the Gemeente Amsterdam is not responsible for any consequences based on the results. Use at your own risk. A thorough understanding of the methods and data is advised in order to make good judgments about what the results may or may not be used for. For details on the methods we refer to the [Technical Documentation](https://onderzoek.amsterdam.nl/publicatie/technische-toelichting) (in Dutch).

## Data
The project mainly uses two data sources: a dataset with regional material flows by the Central Bureau of Statistics (CBS), and a dataset with waste streams by the National Waste Registration (Landelijk Meldpunt Afval, LMA). 
The CBS data specific for Amsterdam is [available](https://onderzoek.amsterdam.nl/dataset/materiaalstromen-in-amsterdam) as part of the Monitor. The same data is available for the regions Noord-Holland and Utrecht through the CBS website. Other regions in the Netherlands can be made available by CBS upon request and for a fee. The dataset uses an aggregation of the European standard _Nomenclature uniforme des marchandises pour les Statistiques de Transport, Revisée_ (NST/R - 1/2 digits). 
The LMA data as it's used in this project is not freely available due to privacy rules. However, it may be requested from the LMA under some conditions. In addition, this raw LMA dataset is first processed by a pipeline created by GeoFluxus. This code is also [freely available](https://github.com/circular-monitor/lma-data-pipeline). Materials in the dataset are labeled according to EURAL-codes (6 digits) or GN codes (8 digits). These are European standards for waste materials, classified by origin (i.e, the industry or business activity in which the waste is released or by type of waste).
For processing and localizing some waste flows we use geographic data with regional coordinates provided by CBS. Download it [here](https://download.cbs.nl/geo/cbsgebiedsindelingen_2022_v1.zip).

**NOTE**: Current version on `develop` does not have working code for waste indicators based on LMA data. Only the material flows dataset provided by the Central Bureau of Statistics is processed. An update to the waste indicators will follow soon.

## Usage
This method works only with the data sets described above. So if you are interested in a Dutch COROP-plus region and the CBS has provided you with data for this specific region, this method is for you. As it is based on many assumptions, we highly recommend getting in touch with the authors if you'd like to use it (j.bosga@amsterdam.nl). Also, we may be able to run the method for you and save you some time. 

To run it yourself, you need a dataset provided by the CBS containing material flows for your COROP-plus region. You also need impact factors for each product group. These can be shared on request (j.bosga@amsterdam.nl). Then, follow these steps:
1. Copy `example_config.yaml` and rename to `config.yaml`.
2. Edit the paths in the config file.
3. OPTIONAL: edit settings in `run.py` to change aggregation level of output or the confidence interval.
3. From the parent directory, call `python monitor_circulaire_economie/run.py`.
4. Resulting indicators file should be in your `PROCESSED-FOLDER`. 

## Contributing
As mentioned in the Disclaimer, there is room for improvement. Any improvements made to our assumptions or our code or other extensions of our methods are welcomed and should be submitted via pull request. 