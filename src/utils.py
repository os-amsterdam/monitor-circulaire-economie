import pandas as pd
import numpy as np
import progressbar

def average(*args):
    args = [arg for arg in args if arg is not None] # Messy to filter out the None here - is caused by trailing comma added in parser. 
    return np.mean(args)

def add_std(x):
    return np.sqrt((x**2).sum())

def attribution_fun(x: pd.DataFrame, key_col: str, num_cols: list, mapping: dict, name: str) -> pd.DataFrame:
    """Helper function to divide the weight of a material group over its constituent material types based on a mapping.

    Args:
        x (pandas.DataFrame): Table containing material groups and weights
        key_col (str): Column to use for mapping
        num_cols (list): Columns to apply the mapping to
        mapping (dict): Dictionary containing the mapping
        name (str): Name of the new column that contains the mapping keys

    Returns:
        pandas.DataFrame: A new table where each material group now has one row for each material type it consists of, and values adjusted according to the mapping.
    """
    row_list = []
    for i, row in x.iterrows():
        map_row = mapping.loc[row[key_col]]
        for map_val, factor in map_row[map_row > 0].items():

            new_row = row.copy()
            new_row[num_cols] *= factor
            new_row[name] = map_val
            row_list.append(new_row)
    return pd.DataFrame(row_list)


def linear_projection(dmi: pd.DataFrame) -> pd.DataFrame:
    """Calculates the reduction in materials needed from 2019 to achieve the 2030 reduction goal, based on a linear pathway.

    Args:
        dmi (pandas.DataFrame): Table containing values for the primary, abiotic DMI over the years.

    Returns:
        pandas.DataFrame: Table with projected DMI values based on the linear pathway to reduction.
        float:  Amount of materials to reduce each year.
    """
    linear_step = (dmi.loc[2019, 'Brutogew'] -
                   (dmi.loc[2016, 'Brutogew'] / 2)) / (2030 - 2019)

    projection_df = pd.DataFrame({'Jaar': np.arange(2019, 2031)})
    projection_df['projectie'] = dmi.loc[2019, 'Brutogew']
    for i, val in projection_df.projectie.iteritems():
        projection_df.loc[i, 'projectie'] = val - i*linear_step

    return projection_df, linear_step


def init_progressbar() -> progressbar.ProgressBar:
    """Initializes a progress bar. 

    Returns:
        progressbar.ProgressBar: Progress bar object.
    """
    widgets = [' [',
                progressbar.Timer(format='elapsed time: %(elapsed)s'),
                '] ',
                progressbar.Bar('*'), ' (',
                progressbar.ETA(), ') ',
                ]

    bar = progressbar.ProgressBar(max_value=24,
                                    widgets=widgets).start()
    return bar