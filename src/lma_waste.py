import pandas as pd
import numpy as np
from pathlib import Path
import json
import os
import geopandas as gpd


class LMAProcessor():
    """This class further processes the LMA dataset after it has been through the pipeline provided by Geofluxus. 
    The resulting table is ready to be used for analysis or as input to the material flow diagram.
    """

    def __init__(self, raw_data_path: str, mappings_folder: str, region_polygons_path: str, save_path: str) -> None:

        self.raw_data_path = raw_data_path
        self.save_path = Path(save_path, 'intermediate/')
        self.mappings_folder = mappings_folder
        self.region_polygons_path = region_polygons_path

    def run(self, region, write: bool = True) -> pd.DataFrame:
        """Executes the processing pipeline and returns the processed table. 

        Args:
            save (bool, optional): Save flag. Defaults to True.

        Returns:
            pandas.DataFrame: Processed LMA table.
        """
        columns = ['EuralCode',  'VerwerkingsmethodeCode', 'Gewicht_KG', 'Verwerker_Adres', 'Herkomst_Adres', 'Verwerker_Location', 'Verwerker_Plaats',
                   'Ontdoener_Plaats', 'Herkomst_Plaats', 'Herkomst_Location', 'Ontdoener_Location', 'EURAL_(a)biotisch', 'EURAL_(an)organisch', 'MeldPeriodeJAAR']
        data = pd.read_csv(self.raw_data_path, header=0, usecols=columns)
        data['EURAL_EURAL_2_cijfer_code'] = data['EuralCode'].astype(
            str).str[:2].astype(int)

        print('Loaded data. Adding secondary waste flag...')
        data = self.add_secondary_waste_flag(data)
        print('Done. Adding address in Amsterdam...')
        data = self.add_address_in_region(data, region, geocoding=True)
        print('Done. Adding Rladder...')
        data = self.add_rladder_feature(data)
        print('Done. Adding material type...')
        data = self.add_material_type(data)
        print('Done. Adding short cycle flag...')
        data = self.add_short_cycle_flag(data)
        print('Done. Saving file.')
        if write:
            if not os.path.exists(self.save_path):
                os.makedirs(self.save_path)
            data.to_csv(Path(self.save_path, 'LMA.csv'))
            
        return data

    def add_secondary_waste_flag(self, data: pd.DataFrame) -> pd.DataFrame:
        """Determines whether waste is primary (e.g. enters the waste processing ecosystem for the first time) or not. 
        This is done based on whether the waste origin is a known waste processing facility or whether it has an EURAL secondary waste tag.

        Args:
            data (pandas.DataFrame): Input LMA table

        Returns:
            pandas.DataFrame: Input LMA table with column 'secundair' added.
        """
        verwerkers_list = data['Verwerker_Adres'].unique()
        data["secundair"] = np.select(
            [(data["EURAL_EURAL_2_cijfer_code"] == 19) | (data["Herkomst_Adres"].isin(verwerkers_list))], [1])
        return data

    def add_address_in_region(self, data, region, geocoding=True) -> pd.DataFrame:
        """Adds flags for whether addresses of processor, producer, and origin are located in a COROPplus region. Can be done by geocoding or by registered location in LMA.

        Args:
            data (pandas.DataFrame): Input LMA data
            geocoding (bool, optional): Flag to use geocoding or LMA registered location. Defaults to True.

        Returns:
            pandas.DataFrame: Input LMA data with columns 'Verwerker_in_regio', 'Ontdoener_in_regio', and 'Herkomst_in_regio' added.
        """

        if geocoding:
            gdf = gpd.read_file(
                self.region_polygons_path, layer='cbs_coropplusgebied_2020_gegeneraliseerd')
            region_polygon = gdf[gdf.statnaam == region]['geometry'].iloc[0]

            data['Verwerker_in_regio'] = self.location_in_region(
                data['Verwerker_Location'], region=region_polygon)
            data['Ontdoener_in_regio'] = self.location_in_region(
                data['Ontdoener_Location'], region=region_polygon)
            data['Herkomst_in_regio'] = self.location_in_region(
                data['Herkomst_Location'], region=region_polygon)

        else:
            # NB: This only works when the COROPplus region is a gemeente
            data['Verwerker_in_regio'] = data['Verwerker_Plaats'] == region.upper()
            data['Ontdoener_in_regio'] = data['Ontdoener_Plaats'] == region.upper()
            data['Herkomst_in_regio'] = data['Herkomst_Plaats'] == region.upper()
        return data

    def location_in_region(self, x: pd.Series, region: list) -> pd.Series:
        """Checks whether coordinates are inside a region or not. 

        Args:
            x (pandas.Series): Series containing coordinates.
            region (list): List containing Shapely geometry for a region.

        Returns:
            pandas.Series: Series with boolean values.
        """
        x = gpd.GeoSeries.from_wkt(x, crs="EPSG:4326")
        x = x.to_crs("EPSG:28992")
        out = x.within(region)
        return out

    def add_rladder_feature(self, data: pd.DataFrame) -> pd.DataFrame:
        """Adds R-strategy tag based on a mapping from processing methods to R-strategies provided by Rijkswaterstaat.

        Args:
            data (pandas.DataFrame): Input LMA data

        Returns:
            pandas.DataFrame: Input LMA data with column 'R_strategie' added.
        """
        with open(Path(self.mappings_folder, 'verwerkingsmethode_rstrategie.json')) as f:
            r_mapping = json.load(f)

        data.loc[:, 'R_strategie'] = data.VerwerkingsmethodeCode.map(r_mapping)
        data.R_strategie.fillna('Onbekend/Tussenstap', inplace=True)

        return data

    def add_material_type(self, data: pd.DataFrame) -> pd.DataFrame:
        """Adds a column containing material type (Metals, Minerals, Fossils, Bio-based). 
        This is done using a manual mapping from EURAL codes for Metals and Minerals. 
        Fossils and Bio-based are determined by the combination of the (an)organic and (a)biotic flags provided by Geofluxus. 
        This approach only has 53% coverage, the remainder is classified as Unknown/Mixed. This could be improved in a future release.

        Args:
            data (pandas.DataFrame): Input LMA data

        Returns:
            pandas.DataFrame: Input LMA data with column 'Grondstoftype' added.
        """
        with open(Path(self.mappings_folder, 'euralcode_metaal_mineraal.json')) as f:
            eural_metaal_mineraal_map = json.load(f)

        data['Grondstoftype'] = data.EuralCode.astype(
            str).map(eural_metaal_mineraal_map)
        biomassa_idx = data[(data['EURAL_(a)biotisch'] == 'biotisch') & (
            data['EURAL_(an)organisch'] == 'organisch')].index
        fossiel_idx = data[(data['EURAL_(a)biotisch'] == 'abiotisch') & (
            data['EURAL_(an)organisch'] == 'organisch')].index

        data.loc[biomassa_idx, 'Grondstoftype'] = 'Biomassa'
        data.loc[fossiel_idx, 'Grondstoftype'] = 'Fossiel'
        data.Grondstoftype.fillna('Onbekend/Gemengd', inplace=True)

        return data

    def add_short_cycle_flag(self, data: pd.DataFrame) -> pd.DataFrame:
        """Adds a flag to indicate short-cycle bio-based waste (such as food), based on a manual EURAL code mapping.
        Currently does not account for the fraction of bio-based waste in municipal and household waste. 
        This could be improved in a future release.

        Args:
            data (pandas.DataFrame): Input LMA data

        Returns:
            pandas.DataFrame: Input LMA data with column 'kortcyclisch' added.
        """
        kortcyclische_eurals = ("200108", "02")

        data.EuralCode = data.EuralCode.astype(str).str.rjust(6, "0")
        data["kortcyclisch"] = data.EuralCode.str.startswith(
            kortcyclische_eurals)
        data.loc[data.kortcyclisch == 1, "Grondstoftype"] = "Biomassa"
        data["EuralCode"] = data["EuralCode"].astype(int)

        return data


    
    def produced_waste(self, data):
        """Calculates the amount of produced _primary_ waste in Amsterdam in kilograms each year.

        Returns:
            pandas.DataFrame: Table with results. 
        """
        return data[(data['secundair'] == False) & (data['Herkomst_in_regio'] == True)][
            ['Gewicht_KG', 'MeldPeriodeJAAR']].groupby(['MeldPeriodeJAAR']).sum()

    def processed_waste(self, data):
        """Calculates the amount of processed _primary_ waste in Amsterdam in kilograms each year.

        Returns:
            pandas.DataFrame: Table with results. 
        """
        return data[(data['secundair'] == False) & (data['Verwerker_in_regio'] == True)][
            ['Gewicht_KG', 'MeldPeriodeJAAR']].groupby(['MeldPeriodeJAAR']).sum()

    def percentage_recycled(self, data):
        """Calculates the percentage of waste that is recycled in Amsterdam each year.

        Returns:
            pandas.DataFrame: Table with results.
        """
        return data[(data['secundair'] == False) & (data['Herkomst_in_regio'] == True) & (data['R_strategie'] == 'R5-Recycle')][
            ['Gewicht_KG', 'MeldPeriodeJAAR']].groupby(['MeldPeriodeJAAR']).sum() / self.produced_waste() * 100

    def percentage_lost(self, data):
        """Calculates the percentage of waste that is lost (either through incineration or landfill) in Amsterdam each year.

        Returns:
            pandas.DataFrame: Table with results.
        """
        return data[(data['secundair'] == False) & (data['Herkomst_in_regio'] == True) & (data['R_strategie'].isin(['R6-Recover', 'Verlies']))][
            ['Gewicht_KG', 'MeldPeriodeJAAR']].groupby(['MeldPeriodeJAAR']).sum() / self.produced_waste() * 100

    def percentage_unknown(self, data):
        """Calculates the percentage of waste of which the destination is unknown in Amsterdam each year. 
        This includes all waste that is processed at more than one waste processing company.

        Returns:
            pandas.DataFrame: Table with results.
        """
        return data[(data['secundair'] == False) & (data['Herkomst_in_regio'] == True) & (data['R_strategie'] == 'Onbekend/Tussenstap')][
            ['Gewicht_KG', 'MeldPeriodeJAAR']].groupby(['MeldPeriodeJAAR']).sum() / self.produced_waste() * 100
