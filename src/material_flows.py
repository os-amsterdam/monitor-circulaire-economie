import numpy as np
import pandas as pd
from pathlib import Path
from utils import attribution_fun


class MaterialFlowDataProcessor():
    """This class is used to generate the table behind the material flow diagram in the Circular Economy Monitor dashboard. 
    It uses data from the CBS material flows dataset and from the dataset by the Landelijk Meldpunt Afval.
    """

    def __init__(self, material_flows_path, waste_data_path, mappings_path, save_path, year=2020) -> None:
        self.material_flows_path = material_flows_path
        self.waste_data_path = waste_data_path
        self.mappings_path = mappings_path
        self.save_path = save_path
        self.year = year

    def run(self, save=True):
        """Executes all processing steps. Returns and optionally saves a table containing the complete set of processed material flows.

        Args:
            save (bool, optional): Save flag. Defaults to True.

        Returns:
            pandas.DataFrame: Dataframe containing material flows.
        """

        flow_data = pd.DataFrame()

        flow_data = self.add_material_flows(flow_data)
        flow_data = self.add_processed_resources(flow_data)
        flow_data = self.add_short_cycle_products(flow_data)
        flow_data['Grondstofgebruik_Voorraden'] = flow_data['Verwerkte grondstoffen_Grondstofgebruik'] - \
            flow_data['Grondstofgebruik_Kortcyclische producten']

        flow_data = flow_data.groupby('Grondstoftype').sum()/1000

        flow_data = self.add_waste_flows(flow_data)
        flow_data = flow_data.fillna(0)

        if save:
            flow_data.to_csv(Path(self.save_path, 'material_flow_diagram.csv'))

        return flow_data

    def preprocess_materials_table(self):
        """Loads and transforms the original CBS data containing material flows. Waste materials are excluded as they are added later from a different source. 
        Material types (Fossils, Biomass, Minerals, and Metals) are attributed according to a mapping based on estimates for all material classes.

        Returns:
            pandas.DataFrame: Table with intermediate results.
        """

        df = pd.read_csv(self.material_flows_path, sep=';', decimal=',')
        df = df[(df.Jaar == self.year)&(df.Gebruiksgroep_naam.isin(['Totaal', 'Niet van toepassing']))][['Stroom', 'Goederengroep_naam', 'Brutogew']]
        flow_data_df = pd.DataFrame(
            index=df.Goederengroep_naam.unique(), columns=df.Stroom.unique())
        for _, row in df.iterrows():
            flow_data_df.loc[row['Goederengroep_naam'],
                             row['Stroom']] = row['Brutogew']

        flow_data_df = flow_data_df.reset_index().rename(
            columns={'index': 'Goederensoort'})
        flow_data_df = flow_data_df.drop(23)  # remove afval
        map_df = pd.read_csv(Path(self.mappings_path, 'gg_grondstoftype.csv'), index_col=0)

        flow_data_df_grondstof = attribution_fun(
            flow_data_df, flow_data_df.columns[0], flow_data_df.columns[1:], map_df, 'Grondstoftype')
        return flow_data_df_grondstof

    def add_material_flows(self, flow_data):
        """Adds nodes to the material flows data that do not involve waste. 

        Args:
            flow_data (pandas.DataFrame): Empty table.

        Returns:
            pandas.DataFrame: Partially filled table with material flows.
        """
        flow_data_df_grondstof = self.preprocess_materials_table()
        flow_data['Goederensoort'] = flow_data_df_grondstof['Goederensoort']
        flow_data['Grondstoftype'] = flow_data_df_grondstof['Grondstoftype']
        flow_data['Import_Wederuitvoer'] = flow_data_df_grondstof['Doorvoer'] + flow_data_df_grondstof['Wederuitvoer']
        flow_data['Import_Verwerkte grondstoffen'] = flow_data_df_grondstof['Invoer_nationaal'] + \
            flow_data_df_grondstof['Invoer_internationaal']
        flow_data['Wederuitvoer_Export'] = flow_data_df_grondstof['Doorvoer'] + flow_data_df_grondstof['Wederuitvoer']
        flow_data['Verwerkte grondstoffen_Export'] = flow_data_df_grondstof['Uitvoer_nationaal'] + \
            flow_data_df_grondstof['Uitvoer_internationaal']
        flow_data['Winning in regio_Verwerkte grondstoffen'] = flow_data_df_grondstof['Aanbod_eigen_regio']
        return flow_data

    def add_processed_resources(self, flow_data):
        """Generates flow data for a specific node (processed resources) in the diagram. 
        
        NB: This currently calculates Processed Resources similar to how the direct DMC is calculated (e.g. import + production - export). 
        This leads to negative values, which are clipped in this method. This leads to a distorted image of resource consumption in the region.
        See `materialenmonitor.src.indicators.materials.dmc_direct` for more info. This will be fixed in a future release. 

        Args:
            flow_data (pandas.DataFrame): Partially filled material flows table.

        Returns:
            pandas.DataFrame: Partial table with Processed Resources flows added.
        """
        # Use a mask for attribution of good streams to a node
        verwerkte_grondstoffen = (flow_data['Winning in regio_Verwerkte grondstoffen'] +
                                  flow_data['Import_Verwerkte grondstoffen']) - flow_data['Verwerkte grondstoffen_Export']
        mask = np.zeros(len(flow_data))
        mask[6] = 0.52  # Steenkool, bruinkool, aardgas, ruwe aardolie
        mask[7] = 0  # cokes en aardolieproducten

        flow_data['Verwerkte grondstoffen_Energetisch verbruik'] = verwerkte_grondstoffen*mask
        flow_data['Verwerkte grondstoffen_Grondstofgebruik'] = verwerkte_grondstoffen - \
            flow_data['Verwerkte grondstoffen_Energetisch verbruik']
        flow_data['Verwerkte grondstoffen_Grondstofgebruik'] = flow_data['Verwerkte grondstoffen_Grondstofgebruik'].clip(
            lower=0)
        flow_data['Verwerkte grondstoffen_Energetisch verbruik'] = flow_data['Verwerkte grondstoffen_Energetisch verbruik'].clip(
            lower=0)
        return flow_data

    def add_short_cycle_products(self, flow_data):
        """Generates flow data for short-lived products (mostly foods) based on a mask.

        Args:
            flow_data (pandas.DataFrame): Partially filled material flows table.

        Returns:
            pandas.DataFrame: Partial table with short-cycle product flows added.
        """
        mask = np.zeros(len(flow_data))
        mask[0] = 1  # Landbouw- en tuinbouwproducten
        mask[2] = 0.49  # Veehouderij-, jacht- en visserijproducten
        mask[3] = 1  # Voedsel
        mask[4] = 1  # Voedsel
        mask[5] = 1  # Voedsel

        flow_data['Grondstofgebruik_Kortcyclische producten'] = flow_data['Verwerkte grondstoffen_Grondstofgebruik'] * mask

        return flow_data

    def add_waste_flows(self, flow_data):
        """Adds primary waste-based material flows to the partial material flow table.  

        Args:
            flow_data (pandas.DataFrame): Partially filled material flows table.

        Returns:
            pandas.DataFrame: Complete material flows table. 
        """
        waste_df = pd.read_csv(self.waste_data_path)
        waste_df = waste_df[waste_df['MeldPeriodeJAAR']==self.year]
        primary_waste_df = waste_df[waste_df.secundair == False]

        flow_data.loc['Onbekend/Gemengd', :] = 0

        flow_data['Kortcyclische producten_Afval'] = primary_waste_df[(primary_waste_df.kortcyclisch == 1) & (
            primary_waste_df.Herkomst_in_regio == True)].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000
        flow_data['Kortcyclische producten_Verlies'] = flow_data['Grondstofgebruik_Kortcyclische producten'] - \
            flow_data['Kortcyclische producten_Afval']

        flow_data['Voorraden_Afval'] = primary_waste_df[(primary_waste_df.Herkomst_in_regio == True) & (
            primary_waste_df.kortcyclisch == 0)].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000
        flow_data['Import afval_Afval'] = primary_waste_df[(primary_waste_df.Herkomst_in_regio == False) & (
            primary_waste_df.Verwerker_in_regio == True)].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000

        flow_data['Afval_Verlies'] = primary_waste_df[(primary_waste_df.Verwerker_in_regio == True) & (primary_waste_df.R_strategie == 'Verlies') & (
            primary_waste_df.kortcyclisch == 0)].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000
        flow_data['Afval_Export afval'] = primary_waste_df[(primary_waste_df.Verwerker_in_regio == False) & (
            primary_waste_df.Herkomst_in_regio == True)].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000

        flow_data['Afval_Recycling'] = primary_waste_df[(primary_waste_df.Verwerker_in_regio == True) & ~(
            primary_waste_df.R_strategie == 'Verlies')].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000
        flow_data['Recycling_Verwerkte grondstoffen'] = primary_waste_df[(primary_waste_df.Verwerker_in_regio == True) & ~(
            primary_waste_df.R_strategie == 'Verlies')].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})/1000000000

        verwerkt_energetisch = primary_waste_df[(primary_waste_df.Verwerker_in_regio == True) & (
            primary_waste_df.R_strategie == 'R6-Recover')].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})['Gewicht_KG']/1000000000
        flow_data['Verwerkte grondstoffen_Energetisch verbruik'] += verwerkt_energetisch.reindex(flow_data.index).fillna(0)
        
        verwerkt_grondstofgebruik = primary_waste_df[(primary_waste_df.Verwerker_in_regio == True) & ~(
            primary_waste_df.R_strategie == 'R5-Recycle')].groupby('Grondstoftype').agg({'Gewicht_KG': 'sum'})['Gewicht_KG']/1000000000
        flow_data['Verwerkte grondstoffen_Grondstofgebruik'] += verwerkt_grondstofgebruik.reindex(flow_data.index).fillna(0)

        return flow_data
