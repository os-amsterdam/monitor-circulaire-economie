import os
import json
import pandas as pd
from pathlib import Path, PosixPath
from monitor_circulaire_economie.src.utils import add_std


class CBSProcessor:
    """Contains functions to calculate several Circular Economy metrics also used in the national Integrale Circulaire Economy Rapportage using a CBS-provided material flows dataset."""

    def __init__(
        self, source, impacts_fpath, mappings_folder: str, destination_folder=None
    ) -> None:
        if (
            isinstance(source, str)
            or isinstance(source, PosixPath)
            or isinstance(source, Path)
        ):
            self.source_path = source
            self.source_data = pd.read_csv(self.source_path, index_col=0, sep=';', decimal=',')
        elif isinstance(source, pd.DataFrame):
            self.source_data = source
        self.impacts_path = impacts_fpath
        self.mappings_folder = mappings_folder
        self.destination_folder = destination_folder

    def generate_indicators(
        self, corrected=True, aggregated=True, CI_level=0.95, write=False
    ) -> pd.DataFrame:
        """Generates several CE KPIs and returns them in a table.

        Args:
            corrected (bool, optional): Flag to apply a correction for certain material groups based on a validation done by Gemeente Amsterdam. Defaults to True.
            aggregated (bool, optional): Flag to aggregate results to year totals. Defaults to True.
            CI_level (float, optional): Confidence level with which to calculate the confidence interval on the DMI. Currently takes .68, .95, and .997 as values (as per the empirical rule). Defaults to .95.
            write (bool, optional): Save flag. Defaults to False.

        Returns:
            pd.DataFrame: Table with results in ktons.
        """

        data = self.source_data.copy()
        fname = "indicators"

        # we do this to calculate CI based on original estimate, not corrected estimate. Is this still reasonable? Validation = more certainty -> therefore adjusted margins? 
        # Not all values are scaled down (e.g. Sand), so scaling the margins by the same factor is weird (validation = larger margins?). But what to adjust the margins to? 
        data["Brutogew_CI"] = data["Brutogew"]
        data["Waarde_CI"] = data["Waarde"]

        if corrected:
            data = self.correct_data(data)
            fname += "_corrected"

        data = self.add_dmi(data)
        data = self.add_dmc(data)
        data = self.add_raw_material_footprint(data)
        data = self.add_primair(data)
        data = self.add_abiotisch(data)
        data = self.add_impacts(data)
        data = self.add_new_labels_and_agendas(data)

        if aggregated:
            data = data.groupby("Jaar").agg(
                {
                    "DMI": "sum",
                    "Waarde": "sum",
                    "DMC": "sum",
                    "RMI": "sum",
                    "Brutogew_CI": "sum",
                    "Waarde_CI": "sum",
                    "Primair gewicht": "sum",
                    "Abiotisch gewicht": "sum",
                    "Primair abiotisch gewicht": "sum",
                    "Totale MKI (Miljoenen euro's)": "sum",
                    "CO2 (kton CO2e)": "sum",
                    "Eco-costs of human health (Miljoenen euro's)": "sum",
                    "Eco-costs of ecotoxicity (Miljoenen euro's)": "sum",
                    "Eco-costs of resource scarcity (Miljoenen euro's)": "sum",
                    "Eco-costs of carbon footprint (Miljoenen euro's)": "sum",
                    "Sf_brutogew": add_std,
                    "Sf_waarde": add_std,
                }
            )
            data = self.generate_CI(data, level=CI_level)
            fname += "_aggregated"
        else:
            data = self.generate_CI(data, level=CI_level)

        data.drop(
            ["Brutogew_CI", "Waarde_CI"],
            axis=1,
            inplace=True,
        )

        if write:
            if not os.path.exists(self.destination_folder):
                os.makedirs(self.destination_folder)
            data.to_csv(Path(self.destination_folder, f"{fname}.csv"))

        return data

    def correct_data(self, data: pd.DataFrame, adjust_margins=False) -> pd.DataFrame:
        """Applies a correction to the CBS data based on manual validation performed by Gemeente Amsterdam. This validation was done for the top 7 largest material groups by mass.

        Args:
            data (pd.DataFrame): Input data to apply correction to.

        Returns:
            pd.DataFrame: Output data with some values corrected.
        """
        with open(Path(self.mappings_folder, "corrections.json")) as f:
            corrections_list = json.load(f)

        for year in data.Jaar.unique():
            for val in corrections_list:
                # get the distribution of weight across use groups and flows
                brutogew = data.loc[
                    (data.Goederengroep_naam == val["product"])
                    & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                    & (data.Jaar == year),
                    "Brutogew",
                ]
                distribution_factor = brutogew / brutogew.sum()

                # distribute corrected value accordingly
                distributed_val = val["value"] * distribution_factor

                # calculate new values based on corrected value and original value, weighed by confidence score
                new_vals = (
                    brutogew * (1 - val["confidence"])
                    + distributed_val * val["confidence"]
                )

                scale_factor = max(
                    new_vals / brutogew
                )  # factor is the same for all rows but may contain NaNs when division by zero. Max circumvents that issue.

                # replace old values with new values
                data.loc[
                    (data.Goederengroep_naam == val["product"])
                    & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                    & (data.Jaar == year),
                    "Brutogew",
                ] = new_vals.values
                data.loc[
                    (data.Goederengroep_naam == val["product"])
                    & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                    & (data.Jaar == year),
                    "Waarde",
                ] *= scale_factor

                if adjust_margins:
                    data.loc[
                        (data.Goederengroep_naam == val["product"])
                        & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                        & (data.Jaar == year),
                        "Sf_brutogew",
                    ] =  data.loc[
                        (data.Goederengroep_naam == val["product"])
                        & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                        & (data.Jaar == year),
                        "Brutogew"]*0.2 # Assumption: validated values have a standard error of 20%

                    data.loc[
                        (data.Goederengroep_naam == val["product"])
                        & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                        & (data.Jaar == year),
                        "Sf_waarde",
                    ] =  data.loc[
                        (data.Goederengroep_naam == val["product"])
                        & (data.Gebruiksgroep_naam.isin(val["gebruikers"]))
                        & (data.Jaar == year),
                        "Waarde"]*0.2 # Assumption: validated values have a standard error of 20%

                # calculate and replace updated totals per flow
                new_totals = (
                    data.loc[
                        (data.Goederengroep_naam == val["product"])
                        & (data.Jaar == year)
                        & (data.Gebruiksgroep_naam != "Totaal")
                        & (
                            data.Stroom.isin(
                                [
                                    "Aanbod_eigen_regio",
                                    "Invoer_nationaal",
                                    "Invoer_internationaal",
                                ]
                            )
                        )
                    ]
                    .groupby("Stroom")["Brutogew"]
                    .sum()
                )

                data.loc[
                    (data.Goederengroep_naam == val["product"])
                    & (data.Jaar == year)
                    & (data.Gebruiksgroep_naam == "Totaal"),
                    "Brutogew",
                ] = new_totals.values

                new_totals = (
                    data.loc[
                        (data.Goederengroep_naam == val["product"])
                        & (data.Jaar == year)
                        & (data.Gebruiksgroep_naam != "Totaal")
                        & (
                            data.Stroom.isin(
                                [
                                    "Aanbod_eigen_regio",
                                    "Invoer_nationaal",
                                    "Invoer_internationaal",
                                ]
                            )
                        )
                    ]
                    .groupby("Stroom")["Waarde"]
                    .sum()
                )

                data.loc[
                    (data.Goederengroep_naam == val["product"])
                    & (data.Jaar == year)
                    & (data.Gebruiksgroep_naam == "Totaal"),
                    "Waarde",
                ] = new_totals.values

        return data

    def generate_CI(self, data: pd.DataFrame, level=0.95) -> pd.DataFrame:
        """Calculates confidence intervals for both mass and value of material flows and adds upper and lower bounds for both to a table.

        Args:
            data (pd.DataFrame): Input table to add columns to.
            level (float, optional): Confidence level with which to calculate the confidence interval on the DMI. Currently takes .68, .95, and .997 as values (as per the empirical rule). Defaults to .95.

        Raises:
            ValueError: Error raised when wrong CI level is passed.

        Returns:
            pd.DataFrame: Output dataframe with CI upper and lower bounds added for mass and value.
        """
        if level == 0.68:
            x = 1
        elif level == 0.95:
            x = 2
        elif level == 0.997:
            x = 3
        else:
            raise ValueError(f"CI level can be .68, .95, or .997. Received {level}.")
        data[f"DMI_CI{str(level)[1:]}_upper"] = (
            data["Brutogew_CI"] + x * data["Sf_gewicht_DMI"]
        )
        data[f"DMI_CI{str(level)[1:]}_lower"] = (
            data["Brutogew_CI"] - x * data["Sf_gewicht_DMI"]
        )
        data[f"Waarde_CI{str(level)[1:]}_upper"] = (
            data["Waarde_CI"] + x * data["Sf_waarde_DMI"]
        )
        data[f"Waarde_CI{str(level)[1:]}_lower"] = (
            data["Waarde_CI"] - x * data["Sf_waarde_DMI"]
        )
        return data

    def add_primair(self, data: pd.DataFrame) -> pd.DataFrame:
        """Applies a scaling factor to the dataset for the fraction of estimated primary materials.

        NB: this scaling factor is taken from national statistics and linearly interpolated over the missing years.
        NB: we assume that this fraction of primary materials applies equally across all materials.
        NB: we assume that this factor applies equally to both weight and value.
        NB: this operation adds uncertainty to the final outcome, but the impact has not yet been quantified.

        Args:
            data (pd.DataFrame): Dataset with material flows.

        Returns:
            pd.DataFrame: Dataset with material flows, scaled down by the estimated fraction of primary materials.
        """
        mapping = pd.read_csv(
            Path(self.mappings_folder, "jaar_primair.csv"), index_col=[0]
        )
        mapping = mapping["0"].to_dict()
        for yr in data.Jaar.unique():
            data.loc[data.Jaar == yr, ["Gewicht_DMI_primair", "Waarde_DMI_primair", "Sf_gewicht_DMI_primair", "Sf_waarde_DMI_primair"]] = (
                data.loc[data.Jaar == yr,  ["Gewicht_DMI", "Waarde_DMI", "Sf_gewicht_DMI", "Sf_waarde_DMI"]] * mapping[yr]
            ).values
        return data

    def add_abiotisch(self, data: pd.DataFrame) -> pd.DataFrame:
        """Applies a scaling factor to the dataset for the fraction of estimated abiotic materials per materials group.

        NB: this scaling factor is based on the national material composition of these material groups.
        NB: we assume that this fraction of abiotic materials applies equally across all nodes in the economy for a materials group.
        NB: we assume that this factor applies equally to both weight and value.
        NB: this operation adds uncertainty to the final outcome, but the impact has not yet been quantified.

        Args:
            data (pd.DataFrame): Dataset with material flows.

        Returns:
            pd.DataFrame: Dataset with material flows, scaled down by the estimated fraction of primary materials.
        """
        mapping_grondstoftype = pd.read_csv(
            Path(self.mappings_folder, "gg_grondstoftype.csv"), index_col=[0]
        )
        mapping_abio = (1 - mapping_grondstoftype["Biomassa"]).fillna(1)
        for gg in mapping_abio.index:
            data.loc[data.Goederengroep_naam == gg, ["Gewicht_DMI_primair_abiotisch", "Waarde_DMI_primair_abiotisch", "Sf_gewicht_DMI_primair_abiotisch", "Sf_waarde_DMI_primair_abiotisch"]] = (
                data.loc[data.Goederengroep_naam == gg, ["Gewicht_DMI_primair", "Waarde_DMI_primair", "Sf_gewicht_DMI_primair", "Sf_waarde_DMI_primair"]]
                * mapping_abio[gg]
            ).values
            data.loc[data.Goederengroep_naam == gg, ["Gewicht_DMI_abiotisch", "Waarde_DMI_abiotisch", "Sf_gewicht_DMI_abiotisch", "Sf_waarde_DMI_abiotisch"]] = (
                data.loc[data.Goederengroep_naam == gg, ["Gewicht_DMI", "Waarde_DMI", "Sf_gewicht_DMI", "Sf_waarde_DMI"]] * mapping_abio[gg]
            ).values
        return data

    def add_dmi(self, data: pd.DataFrame) -> pd.DataFrame:
        """Calculates the Direct Material Input. DMI represents the total material throughput or material scale of an economy.
        DMI is defined as imports + local extraction of raw materials.

        Args:
            data (pd.DataFrame): Input dataframe to append column to.

        Returns:
            pd.DataFrame: Output dataframe with DMI column added.
        """
        # TODO: add version with Wederuitvoer

        flows = [
            "Aanbod_eigen_regio",
            "Invoer_nationaal",
            "Invoer_internationaal",
            "Uitvoer_nationaal",
            "Uitvoer_internationaal",
        ]
        use_groups = [
            "Consumptie huishoudens",
            "Consumptie overheid en investeringen",
            "Dienstverlening bedrijven",
            "Productie goederen",
            "Niet van toepassing",
        ]
        raw_materials = [
            "Land- en tuinbouwproducten",
            "Veehouderij-, jacht- en visserijproducten",
            "Bosbouwproducten",
            "Zout, zand, grind, klei",
            "Ertsen",
            "Steenkool, bruinkool, aardgas en ruwe aardolie",
        ]

        data = data[
            (data.Stroom.isin(flows)) & (data.Gebruiksgroep_naam.isin(use_groups))
        ]
        data.loc[
            data.Gebruiksgroep_naam == "Niet van toepassing", "Gebruiksgroep_naam"
        ] = "Winning voor export"
        data = data.loc[
            ~(
                (~data.Goederengroep_naam.isin(raw_materials))
                & (data.Gebruiksgroep_naam == "Winning voor export")
            )
        ]
        # data = (
        #     data.groupby(["Jaar", "Goederengroep_naam", "Gebruiksgroep_naam"])
        #     .agg(
        #         {
        #             "Brutogew": "sum",
        #             "Waarde": "sum",
        #             "Brutogew_CI": "sum",
        #             "Waarde_CI": "sum",
        #             "Sf_brutogew": add_std,
        #             "Sf_waarde": add_std,
        #         }
        #     ) 
        #     .reset_index()
        # )
        data = data[["Jaar", "Goederengroep_naam", "Stroom", "Gebruiksgroep_naam", "Brutogew", "Waarde", "Brutogew_CI", "Waarde_CI", "Sf_brutogew" , "Sf_waarde"]]
        data.rename(columns={"Brutogew": "Gewicht_DMI", "Waarde": "Waarde_DMI", "Sf_brutogew":"Sf_gewicht_DMI", "Sf_waarde":"Sf_waarde_DMI"}, inplace=True)
        return data

    def add_dmc(self, data: pd.DataFrame) -> pd.DataFrame:
        """Calculates the Direct Material Consumption and adds it as a column to the data.
        The DMC is defined by all materials consumed by households and government, and by businesses providing services.

        Args:
            data (pd.DataFrame): Input dataframe to append column to.

        Returns:
            pd.DataFrame: Output dataframe with DMC column added.
        """
        use_groups = [
            "Consumptie huishoudens",
            "Consumptie overheid en investeringen",
            "Dienstverlening bedrijven",
        ]
        # with_dienstverlening = [
        #     "Zout, zand, grind, klei",
        #     "Metaalproducten",
        #     "Bosbouwproducten",
        #     "Overige minerale producten",
        #     "Land- en tuinbouwproducten",
        #     "Voedsel, plantaardig",
        #     "Voedsel, dierlijk",
        #     "Voedsel, overig",
        # ]
        data[["Gewicht_DMC", "Waarde_DMC", "Sf_gewicht_DMC", "Sf_waarde_DMC"]] = data[["Gewicht_DMI", "Waarde_DMI", "Sf_gewicht_DMI", "Sf_waarde_DMI"]]
        data.loc[
            (~data.Gebruiksgroep_naam.isin(use_groups))
            | (data.Goederengroep_naam == "Afval"),
            ["Gewicht_DMC", "Waarde_DMC", "Sf_gewicht_DMC", "Sf_waarde_DMC"]
        ] = 0

        # Decided to include all materials consumption by service-oriented businesses 
        # data.loc[
        #     ~(data.Goederengroep_naam.isin(with_dienstverlening))
        #     & (data.Gebruiksgroep_naam == "Dienstverlening bedrijven"),
        #     "DMC",
        # ] = 0
        return data

    def add_raw_material_footprint(self, data: pd.DataFrame) -> pd.DataFrame:
        """Calculates the Raw Material Input (RMI) and adds it as a column to the data.
        RMI is defined as the DMI multiplied by a raw material factor per material type (Biomass, Minerals, Metals, Fossils).

        NB: the raw material factors are taken from [national estimates](https://www.cbs.nl/-/media/_excel/2021/03/indicatoren-icer.xlsx) and may not accurately reflect the local situation.
        NB: we apply these raw material factors based on a mapping from material groups to material types, which is based on the national material composition of these material groups.

        Args:
            data (pd.DataFrame): Input dataframe to append column to.

        Returns:
            pd.DataFrame: Output dataframe with RMI column added.
        """
        grondstoftype_rmi_mapping = pd.read_csv(
            Path(self.mappings_folder, "grondstoftype_rmi.csv"), index_col=[0]
        )["0"]
        gg_grondstoftype_mapping = pd.read_csv(
            Path(self.mappings_folder, "gg_grondstoftype.csv"), index_col=[0]
        )
        gg_rmi_mapping = (gg_grondstoftype_mapping * grondstoftype_rmi_mapping).sum(
            axis=1
        )
        for gg in gg_rmi_mapping.index:
            data.loc[data.Goederengroep_naam == gg, ["Gewicht_RMI", "Waarde_RMI", "Sf_gewicht_RMI", "Sf_waarde_RMI"]] = (
                data.loc[data.Goederengroep_naam == gg, ["Gewicht_DMI", "Waarde_DMI", "Sf_gewicht_DMI", "Sf_waarde_DMI"]] * gg_rmi_mapping[gg]
            ).values
            data.loc[data.Goederengroep_naam == gg, ["Gewicht_RMC", "Waarde_RMC", "Sf_gewicht_RMC", "Sf_waarde_RMC"]] = (
                data.loc[data.Goederengroep_naam == gg, ["Gewicht_DMC", "Waarde_DMC", "Sf_gewicht_DMC", "Sf_waarde_DMC"]] * gg_rmi_mapping[gg]
            ).values

        return data

    def add_impacts(self, data: pd.DataFrame) -> pd.DataFrame:
        """Loads impacts per kg per product group and adds these factors to the input dataframe.
        It also multiplies these factors with the DMC to obtain total environmental costs for each indicator for each row.

        Args:
            data (pd.DataFrame): Input dataframe to append columns to.

        Returns:
            pd.DataFrame: Output dataframe with columns added for impacts (both per kg and totals)
        """

        impacts = pd.read_csv(self.impacts_path, index_col=0)
        data = pd.merge(
            data,
            impacts.reset_index(),
            how="left",
            left_on="Goederengroep_naam",
            right_on="product_group",
        )
        data["Totale MKI (Miljoenen euro's)"] = (
            data["Gewicht_DMC"] * data["Totale MKI (euro / kg)"]
        )
        data["CO2 (kton CO2e)"] = data["Gewicht_DMC"] * data["CO2 (kg CO2e/kg)"]
        data["Eco-costs of human health (Miljoenen euro's)"] = (
            data["Gewicht_DMC"] * data["eco-costs of human health (eur/kg)"]
        )
        data["Eco-costs of ecotoxicity (Miljoenen euro's)"] = (
            data["Gewicht_DMC"] * data["eco-costs of ecotoxicity (eur/kg)"]
        )
        data["Eco-costs of resource scarcity (Miljoenen euro's)"] = (
            data["Gewicht_DMC"] * data["eco-costs of resource scarcity (eur/kg)"]
        )
        data["Eco-costs of carbon footprint (Miljoenen euro's)"] = (
            data["Gewicht_DMC"] * data["eco-costs of carbon footprint (eur/kg)"]
        )
        return data
    
    def add_new_labels_and_agendas(self, data: pd.DataFrame) -> pd.DataFrame:
        """Adds some extra columns to the data based on a mapping from product groups to more readable product groups and transition agendas.

        Args:
            data (pd.DataFrame): Input dataframe with indicators

        Returns:
            pd.DataFrame: Output dataframe with columns added
        """
        product_group_map = pd.read_csv(Path(self.mappings_folder,"gg_dmc_transitieagenda_new_groups.csv"), index_col=0)
        data["Goederengroep_nieuw_14"] = data.Goederengroep_naam.map(product_group_map["Goederensoorten_14"])
        data["Goederengroep_nieuw_18"] = data.Goederengroep_naam.map(product_group_map["Goederensoorten_18"])
        data["TransitieAgenda_DMC"] = data.Goederengroep_naam.map(product_group_map["Sectoren en ketens (transitieagenda\'s)"])
        return data