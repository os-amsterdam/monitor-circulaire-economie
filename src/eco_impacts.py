import math
import pandas as pd
import re
from pathlib import Path
import openpyxl
from openpyxl import load_workbook
from pycel.excelformula import ExcelFormula
from utils import average

class EcoProcessor():
    """
    This class combines the environmental impacts of the reference products used in our estimation method with their respective contributions to the goods group they represent.
    This information is mostly already present in a master Excel file, so this class parses the information in that file.

    Parser steps:
        1. Read in each product group sheet as a dataframe
        2. Add extra columns for the impacts we're interested in
        3. Resolve and evaluate references to other cells in the excel workbook
        4. Weigh impacts of reference products 
        5. Add coverage for each product group
    """        

    def __init__(self, gf_data_path, save_folder) -> None:
        self.save_folder = save_folder
        self.idemat = pd.read_excel(gf_data_path, sheet_name='Idemat2022')
        self.agri_footprint = pd.read_excel(gf_data_path, sheet_name='agri-footprint')
        self.wflca = pd.read_excel(gf_data_path, sheet_name='World Food LCA')
        self.wb = load_workbook(gf_data_path)
        self.impact_cols = ['Idemat cost per unit (Euro / kg)', 'CO2 (kg CO2e/kg)', 
       'eco-costs of human health (eur/kg)',
       'eco-costs of ecotoxicity (eur/kg)',
       'eco-costs of resource scarcity (eur/kg)',
       'eco-costs of carbon footprint (eur/kg)']
        self.current_sheet_df = None

    def get_impacts_per_product_group(self, include_import=True, include_export=True, write=False) -> pd.DataFrame:
        """Calculates impacts for each product group and saves the results in a table.

        Args:
            include_import (bool, optional): Whether to use imported products in the calculation. Defaults to True.
            include_export (bool, optional): Whether to use exported products in the calculation. Defaults to True.
            write (bool, optional): Save flag. Defaults to False.

        Returns:
            pd.DataFrame: Table with impacts per product group.
        """
        results = []
        for sheet in self.wb.worksheets[6:-1]: # for use with edited version
            df = self.parse_sheet(sheet)
            impacts = self.get_impacts_kg(sheet, df, include_import, include_export)
            impacts['coverage'] = self.get_coverage(sheet, df, include_import, include_export)
            impacts['product_group'] = sheet['B2'].value
            results.append(impacts)
        impacts_per_product_group = pd.DataFrame(results).set_index('product_group')
        impacts_per_product_group.rename(columns={'Idemat cost per unit (Euro / kg)': 'Totale MKI (euro / kg)'}, inplace=True)
        if write:
            impacts_per_product_group.to_csv(Path(self.save_folder, 'impacts_kg_product_group.csv'))
        return impacts_per_product_group

    def parse_sheet(self, sheet:openpyxl.worksheet.worksheet.Worksheet) -> pd.DataFrame:
        """Converts an Excel worksheet to a dataframe by parsing and evaluating the Excel formulas. 

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Excel worksheet.

        Returns:
            pd.DataFrame: Table with cells evaluated.
        """
        df = self.generate_dataframe(sheet)
        self.current_sheet_df = df

        for i, col in enumerate(self.impact_cols[2:]):
            df[col] = df['Idemat cost per unit (Euro / kg)'].apply(self.parse_references, args=(i+1,))
            df[col] = df[col].apply(self.evaluate_statement)


        for col in self.impact_cols[:2]: 
            df[col] = df[col].apply(self.parse_references)
            df[col] = df[col].apply(self.evaluate_statement)

        out_df = df.dropna(subset=['CO2 (kg CO2e/kg)']).copy()
        out_df.loc[out_df['import/export']=='import', 'Weight'] = out_df.loc[out_df['import/export']=='import', 'Import'] / out_df.loc[out_df['import/export']=='import', 'Import'].sum()
        out_df.loc[out_df['import/export']=='export', 'Weight'] = out_df.loc[out_df['import/export']=='export', 'Export'] / out_df.loc[out_df['import/export']=='export', 'Export'].sum()

        return out_df

    def generate_dataframe(self, sheet:openpyxl.worksheet.worksheet.Worksheet) -> pd.DataFrame:
        """Converts an Excel worksheet to a pandas DataFrame.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Excel worksheet.

        Returns:
            pd.DataFrame: Resulting table.
        """
        # NB!! Worksheet 3 in geofluxus file has an extra empty column K. Quick workaround is to delete this column in the source file. Same for worksheet 17.
        rng = self.get_range(sheet)
        val_list = []
        for value in sheet.iter_rows(min_row=rng[0],
                                    max_row=rng[1],
                                    min_col=2,
                                    max_col=19,                              
                                    values_only=True):
            val_list.append(value)

        columns = ["Product/flow","Import", "Export", "NST code", "Idemat cost per unit (Euro / kg)", "CO2 (kg CO2e/kg)", "Total impact", "CO2 emissions"]
        df = pd.DataFrame(val_list)
        df_import = df.iloc[:, :8]
        df_export = df.iloc[:, 10:]
        df_import.columns = columns
        df_import['import/export'] = 'import'
        df_export.columns = columns
        df_export['import/export'] = 'export'
        df = pd.concat([df_import, df_export], axis=0)
        df = df.reset_index(drop=True)

        return df


    def get_impacts_kg(self, sheet:openpyxl.worksheet.worksheet.Worksheet, df:pd.DataFrame, i=True, e=True) -> pd.DataFrame:
        """Calculates impacts per kg by taking a weighted average of the reference product impacts. 
        Weight is based on national trade volumes for import, export, or both.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet
            df (pd.DataFrame): Current dataframe with impacts.
            i (bool, optional): Flag to include imported products. Defaults to True.
            e (bool, optional): Flag to include exported products. Defaults to True.

        Returns:
            pd.DataFrame: Resulting dataframe with impacts / kg per impact category.
        """
        total_export = sheet[f"B{self.search_value_in_column(sheet, 'Total export')}"].value
        total_import = sheet[f"B{self.search_value_in_column(sheet, 'Total import')}"].value
        if i:
            impact = impact_i = (df.loc[df['import/export']=='import'][self.impact_cols].T * df.loc[df['import/export']=='import']['Weight']).T.sum()
        if e:
            impact = impact_e = (df.loc[df['import/export']=='export'][self.impact_cols].T * df.loc[df['import/export']=='export']['Weight']).T.sum()
        if i and e:
            impact = impact_i * (total_import/(total_import+total_export)) + impact_e*(total_export / (total_import+total_export))
        return impact

    def get_range(self, sheet:openpyxl.worksheet.worksheet.Worksheet)->list:
        """Returns a range of indices with reference products.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet.

        Returns:
            list: Range of indices.
        """
        import_i = self.find_last_product_cell(sheet, 'B')
        export_i = self.find_last_product_cell(sheet, 'L')
        return [6, max(import_i, export_i)]
        
    def find_last_product_cell(self, sheet:openpyxl.worksheet.worksheet.Worksheet, col:str)->int:
        """Finds the last row with a value in a column.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet.
            col (str): Column to search.

        Returns:
            int: Index of last item.
        """
        i = 6 #products start at row 6
        end=False
        while not end:
            value = sheet[f"{col}{i}"].value
            if value==None:
                end=True
            else:
                i+=1
        return i-1

    def search_value_in_column(self, ws:openpyxl.worksheet.worksheet.Worksheet, search_string:str, column="A")->int:
        """Searches for a value in a column and returns the row index.

        Args:
            ws (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet.
            search_string (str): String to search for.
            column (str, optional): Column to search. Defaults to "A".

        Returns:
            int: Row index of value.
        """
        for row in range(1, ws.max_row + 1):
            coordinate = "{}{}".format(column, row)
            if ws[coordinate].value == search_string:
                return row
        return None

    def get_coverage(self, sheet:openpyxl.worksheet.worksheet.Worksheet, df:pd.DataFrame, i=True, e=True) -> float:
        """Calculates coverage of reference products, based on trade volume represented. 
        NB: we calculate coverage slightly differently from Geofluxus. They calculate separate coverages for import and export and weigh them evenly. 
        We calculate coverage over the total trade. This way, there is no bias from an imbalance in import/export trade volume.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet.
            df (pd.DataFrame): Current dataframe with impacts.
            i (bool, optional): Whether to include imported products. Defaults to True.
            e (bool, optional): Whether to include exported products. Defaults to True.

        Returns:
            float: Coverage.
        """
        total_export = sheet[f"B{self.search_value_in_column(sheet, 'Total export')}"].value
        total_import = sheet[f"B{self.search_value_in_column(sheet, 'Total import')}"].value
        if i:
            covered_import = df[df['import/export']=='import']['Import'].sum()
            coverage = covered_import/total_import*100
        if e:
            covered_export = df[df['import/export']=='export']['Export'].sum()
            coverage = covered_export/total_export*100
        if i and e:
            coverage = (covered_import+covered_export)/(total_import+total_export)*100
        return coverage
        
    def replace_column(self, x:str, offset:int) -> str:
        """Shifts column to include additional impact categories from the impact databases.

        Args:
            x (str): Column letter.
            offset (int): Offset specific to the required impact category (impacts are in adjacent columns in database).

        Returns:
            str: Shifted column letter.
        """
        if x == None:
            return x
        elif 'Idemat' in x:
            cols = 'GHIJK'
            x = x.replace('!G', f'!{cols[offset]}')
        elif 'agri-footprint' in x or 'World Food LCA' in x:
            cols = 'CDEFG'
            x = x.replace('!C', f'!{cols[offset]}')
        return x

    def letter_to_idx(self, col:str) -> str:
        """Converts letter to index to facilitate conversion between Excel and pandas.

        Args:
            col (str): Column letter.

        Returns:
            int: Column index.
        """
        l = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        return l.find(col)

    def lookup(self, ref:str, col_offset:int) -> str:
        """Finds the value that an Excel cell refers to.

        Args:
            ref (str): Excel cell contents.
            col_offset (int): Columns offset used to get the right impact column in the database.

        Returns:
            str: Resulting value in string format.
        """
        if '!' in ref:
            sheet, cell = ref.split('!')
        else:
            sheet, cell = None, ref
            
        if ':' in cell:
            return self.lookup_range(sheet, cell, col_offset)
        else:
            return self.lookup_cell(sheet, cell, col_offset)

    def lookup_range(self, sheet:openpyxl.worksheet.worksheet.Worksheet, cell:str, col_offset:int) -> str:
        """Looks up values from a range reference in Excel.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet
            cell (str): Cell contents.
            col_offset (int): Columns offset used to get the right impact column in the database.

        Returns:
            str: Resulting values in string format.
        """
        start, end = cell.split(':')
        i_s = int(start[1:])
        i_e = int(end[1:])
        res = ''
        while i_s <= i_e:
            cell = f"{start[0]}{i_s}"
            val = self.lookup_cell(sheet, cell, col_offset)
            if math.isnan(val):
                pass
            else:
                res += str(val)
                res +=','
            i_s+=1
        return res[:-1] # remove trailing comma

    def lookup_cell(self, sheet:openpyxl.worksheet.worksheet.Worksheet, cell:str, col_offset:int) -> str:
        """Looks up a value from a cell reference in Excel.

        Args:
            sheet (openpyxl.worksheet.worksheet.Worksheet): Current Excel worksheet
            cell (str): Cell contents.
            col_offset (int): Columns offset used to get the right impact column in the database.

        Returns:
            str: Resulting values in string format.
        """
        col, row = cell[0], int(cell[1:])
        col = self.letter_to_idx(col)
        row-=2 # pandas is 0-indexed, excel is 1-indexed, and header doesn't count in pandas
        if sheet==None:
            col -= 1
            row -= 4 # extra offset in product group sheets
            return self.current_sheet_df.iloc[row, col]
        elif sheet=='Idemat2022':
            return self.idemat.iloc[row, col+col_offset]
        elif sheet=="'agri-footprint'":
            return self.agri_footprint.iloc[row, col+col_offset]
        elif sheet=="'World Food LCA'":
            return self.wflca.iloc[row, col+col_offset]
        else:
            val = self.wb[sheet.strip("'")][cell].value
            if re.search(r"(?<!!|:)[A-Z]\d{1,4}", val): 
                return f"{sheet}!{val[1:]}" # include sheet if it's a same-sheet reference in a sheet that's not the current sheet
            else:
                return val


    def parse_string(self, pattern:str, string:str, col_offset:int) -> str:
        """Parses Excel cell contents and replaces references with the corresponding values. 
        It does this by iterating through the string and matching predefined patterns which are then looked up.

        Args:
            pattern (str): Pattern to match. 
            string (str): String to parse.
            col_offset (int): Columns offset used to get the right impact column in the database.

        Returns:
            str: String with values.
        """
        done=False
        res = ''
        pos = 0
        i=0
        original = string
        while not done:
            i+=1
            original = original[pos:]
            m = re.search(pattern,original)
            if m is None:
                res+=original
                done= True
                break
            val = self.lookup(m.group(), col_offset)
            res += original[0:m.start()]

            res += str(val)
            pos = m.end()
        return res
        
        
    def parse_references(self, row, col_offset=0):
        """Main function used to parse Excel cells and extract the values from cell references. There are several different types of references, which are processed in sequence. These are: same-sheet references, references to another product sheet, and references to one of the impact databases. References may be chained.

        Args:
            row (_type_): String to parse.
            col_offset (int, optional): Columns offset used to get the right impact column in the database. Defaults to 0.

        Returns:
            str: Parsed string containing values.
        """
        if row == None or 'DATA NOT USED' in row:
            return None
        else:
            prev_res = row
            res = self.parse_string(r"(?<!!|:)[A-Z]\d{1,4}", prev_res, col_offset)
            res = self.parse_string(r"'\d{1,2}'![A-Z]\d{1,4}", res, col_offset)
            res = self.parse_string(r"(((Idemat2022|'agri-footprint'|'World Food LCA')![A-Z]\d{1,4}:[A-Z]\d{1,4})|((Idemat2022|'agri-footprint'|'World Food LCA')![A-Z]\d{1,4}))", res, col_offset)
            while prev_res != res: # for chained in-sheet references
                prev_res = res
                res = self.parse_string(r"(?<!!|:)[A-Z]\d{1,4}", prev_res, col_offset)
                res = self.parse_string(r"'\d{1,2}'![A-Z]\d{1,4}", res, col_offset)
                res = self.parse_string(r"(((Idemat2022|'agri-footprint'|'World Food LCA')![A-Z]\d{1,4}:[A-Z]\d{1,4})|((Idemat2022|'agri-footprint'|'World Food LCA')![A-Z]\d{1,4}))", res, col_offset)
            
            res = res[0] + res[1:].replace('=', '')
            return res

    def evaluate_statement(self, row:str)-> float:
        """Function to evaluate the parsed strings as python code.

        Args:
            row (str): Parsed Excel strings.

        Returns:
            float: Resulting value. 
        """
        if row ==None:
            return None
        else:
            f = ExcelFormula(row)
            return eval(f.python_code)






