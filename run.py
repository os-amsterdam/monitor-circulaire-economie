import yaml
from src.cbs_materials import CBSProcessor

with open("monitor_circulaire_economie/config.yaml", "r") as f:
    config = yaml.safe_load(f)

cbs_prep = CBSProcessor(
    config["CBS-SOURCE"],
    config["IMPACTS-SOURCE"],
    config["MAPPINGS-FOLDER"],
    config["PROCESSED-FOLDER"],)

indicators = cbs_prep.generate_indicators(aggregated=False, CI_level=.68, write=True, corrected=False)

